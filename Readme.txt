## CYCLO CAFFEINE CLUB
Le site est une seule page, c’est un scrollant qu’on passe d’une partie à l’autre. 
À chaque « scrollage», les différentes parties apparaissent en fadding.
Homepage :
Logo CCC cliquable qui ramène automatiquement sur la page d’accueil.
Pour la navigation, au survol, la typographie change de graisse (elle passe de medium à heavy).
Les CTA « tester » et « réserver » amènent vers des liens externe.
Les pictos Facebook et Instagram sont des liens externes qui mènent vers les réseaux sociaux du CCC.
La flèche qui se trouve en bas indique qu’il faut scroller, il faut donc la mettre en mouvement comme si elle rebondissait.
La pagination qui se trouve en bas à droite s’adapte en fonction de la partie sur laquelle on se trouve. Possibilité 
également en cliquand d’aller d’une partie à l’autre.
Vidéo en arrière plan qui fait une boucle.
Page Concept :
En ce qui concerne le logo, la navigation, les CTA, les pictos, la flèche et la pagination, tout cela reste fixe. Et ceux 
pour toutes les parties.
En haut à gauche, la vidéo de Joris. CTA play grossit au survol.
Le CTA « je veux tester ! » amène au même lien externe que le CTA « tester » qui se trouve dans la barre de navigation 
en haut à droite.
Le CTA «découvrir crossfit minimes » amène vers un lien externe.
Page Interviews :
En ce qui concerne le logo, la navigation, les CTA, les pictos, la flèche et la pagination, tout cela reste fixe. Et ceux 
pour toutes les parties.
Vidéos qui peuvent être changées par la suite. CTA play grossissent au survol. La vidéo s’ouvre en pop-up au centre 
de la page. En cliquand autour de la vidéo permet de la fermer.
Page Tarifs :
En ce qui concerne le logo, la navigation, les CTA, les pictos, la flèche et la pagination, tout cela reste fixe. Et ceux 
pour toutes les parties.
Le CTA «découvrir crossfit » amène vers un lien externe.
Page Planning :
En ce qui concerne le logo, la navigation, les CTA, les pictos, la flèche et la pagination, tout cela reste fixe. Et ceux 
pour toutes les parties.
Le planning pourra être une image, à voir comment on gère la version mobile.
Page contact :
En ce qui concerne le logo, la navigation, les CTA, les pictos, la flèche et la pagination, tout cela reste fixe. Et ceux 
pour toutes les parties.
Le formulaire de contact renvoie à un e-mail.